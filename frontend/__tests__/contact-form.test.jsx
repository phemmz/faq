import { render, fireEvent } from '@testing-library/react';
import ContactForm from 'pages/contact-form';

const FORM_VALUES = {
  order: 'QXAOP',
  name: 'Joe Ben',
  phone: '0901230912',
  email: 'jb@gmail.com',
  message: 'Hello, need help with this.'
}

describe('Contact Us Form', () => {
  it('renders contact form', () => {
    const { getByTestId } = render(<ContactForm />);
    const form = getByTestId('contact-form');

    expect(form).toBeInTheDocument();
  });

  it('submits new contact form', () => {
    const { getByLabelText, getByText } = render(<ContactForm />);
    const orderInput = getByLabelText(/Order/i)
    const nameInput = getByLabelText(/Name/i)
    const phoneInput = getByLabelText(/Phone/i)
    const emailInput = getByLabelText(/Email/i)
    const messageInput = getByLabelText(/Message/i)

    fireEvent.change(orderInput, { target: { value: FORM_VALUES.order } });
    fireEvent.change(nameInput, { target: { value: FORM_VALUES.name } });
    fireEvent.change(phoneInput, { target: { value: FORM_VALUES.phone } });
    fireEvent.change(emailInput, { target: { value: FORM_VALUES.email } });
    fireEvent.change(messageInput, { target: { value: FORM_VALUES.message } });

    expect(orderInput.value).toBe(FORM_VALUES.order);
    expect(nameInput.value).toBe(FORM_VALUES.name);
    expect(phoneInput.value).toBe(FORM_VALUES.phone);
    expect(emailInput.value).toBe(FORM_VALUES.email);
    expect(messageInput.value).toBe(FORM_VALUES.message);
    fireEvent.click(getByText(/Submit/i));
  });
});
