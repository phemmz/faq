import { render } from '@testing-library/react';
import Home from 'pages/index';

describe('Home', () => {
  it('renders a heading', () => {
    const { getByText } = render(<Home />);
    const heading = getByText(/Help Center/i);

    expect(heading).toBeInTheDocument();
  });

  it('renders the option cards', () => {
    const { getAllByTestId, getByText } = render(<Home />);
    const cards = getAllByTestId('option-card');
    const contactCard = getByText(/Contact Form/i);
    const orderCard = getByText(/Where is my Order?/i);

    expect(cards).toHaveLength(2);
    expect(contactCard).toBeInTheDocument();
    expect(orderCard).toBeInTheDocument();
  });
});
