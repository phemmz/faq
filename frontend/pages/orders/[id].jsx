import React from 'react';
import Layout from 'components/Layout';
import Details from 'components/Orders/Details';
import { getOrders, postPageView } from 'utils/api';

export default function OrderDetails({ order }) {
  return (
    <Layout>
      <Details order={order} />
    </Layout>
  );
}

export async function getStaticPaths() {
  const orders = await getOrders();

  const paths = orders.map(({ reference }) => ({
    params: { id: reference },
  }));

  return { paths, fallback: false };
}

export async function getStaticProps({ params }) {
  await postPageView({ path: `/orders/${params.id}` });
  const orders = await getOrders();
  const order = orders.find(({ reference }) => reference === params.id);

  return {
    props: {
      order,
    },
  };
}
