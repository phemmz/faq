import React from 'react';
import Layout from 'components/Layout';
import OrderList from 'components/Orders/OrderList';
import { getOrders, postPageView } from 'utils/api';

export default function Orders({ allOrders }) {
  return (
    <Layout>
      <OrderList allOrders={allOrders} />
    </Layout>
  );
}

export async function getStaticProps() {
  const orders = await getOrders();
  await postPageView({ path: '/orders' });

  return {
    props: {
      allOrders: orders,
    },
  }
}
