import 'bootstrap/dist/css/bootstrap.min.css';
import 'nextjs-breadcrumbs/dist/index.css';

export default function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}
