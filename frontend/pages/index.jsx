import React from 'react';
import Layout from 'components/Layout';
import Home from 'components/Home';
import { postPageView } from 'utils/api';

export default function App() {
  return (
    <Layout omitRootLabel>
      <Home />
    </Layout>
  );
}

export async function getStaticProps() {
  await postPageView({ path: '/' });

  return {
    props: {},
  }
}
