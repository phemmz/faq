import React from 'react';
import Layout from 'components/Layout';
import ContactForm from 'components/ContactForm';
import { postPageView } from 'utils/api';

export default function Contact() {
  return (
    <Layout>
      <ContactForm />
    </Layout>
  );
}

export async function getStaticProps() {
  await postPageView({ path: '/contact-form' });

  return {
    props: {},
  }
}
