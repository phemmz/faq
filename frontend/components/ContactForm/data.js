export const FORM_FIELDS = [
  {
    name: 'orderNumber',
    type: 'text',
    label: 'Order',
    placeholder: 'Enter order number',
    required: false,
  },
  {
    name: 'fullName',
    type: 'text',
    label: 'Name',
    placeholder: 'Enter full name',
    required: true,
  },
  {
    name: 'phoneNumber',
    type: 'tel',
    label: 'Phone',
    placeholder: 'Enter phone number',
    required: true,
  },
  {
    name: 'email',
    type: 'email',
    label: 'Email',
    placeholder: 'Enter email address',
    required: true,
  },
  {
    name: 'message',
    component: 'textarea',
    rows: 3,
    label: 'Message',
    placeholder: 'Enter your message',
    required: true,
  },
];
