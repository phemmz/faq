import React, { useRef, useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import ReCAPTCHA from 'react-google-recaptcha';
import SuccessModal from 'components/ContactForm/SuccessModal';
import { FORM_FIELDS } from 'components/ContactForm/data';
import { submitContactForm } from 'utils/api';

const getInitialState = (fields) => (
  fields.reduce((acc, curr) => {
    return {
      ...acc,
      [curr.name]: ''
    }
  }, {})
);

const initialState = getInitialState(FORM_FIELDS);

export default function ContactForm() {
  const [inputValues, setInputValues] = useState(initialState);
  const [validated, setValidated] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);
  const [showModal, setModalShow] = useState(false);
  const recaptchaRef = useRef();

  const onChange = (event) => {
    const { name, value } = event.target;

    setInputValues(state => ({
      ...state,
      [name]: value,
    }));
  }

  const onSubmit = async (event) => {
    try {
      const form = event.currentTarget;
      event.preventDefault();

      if (form.checkValidity()) {
        setBtnLoading(true);
        await submitContactForm(inputValues);
        recaptchaRef.current.execute();
        setBtnLoading(false);
        setModalShow(true);
        setValidated(false);
        setInputValues(initialState);
      } else {
        setValidated(true);
      }
    } catch (error) {
      setBtnLoading(false);
      setErrorMessage(error.message);
    }
  }

  const onReCAPTCHAChange = (captchaCode) => {
    if(!captchaCode) {
      return;
    }

    recaptchaRef.current.reset();
  }

  const onClose = () => {
    setModalShow(false);
  }

  return (
    <Form className="mt-5" onSubmit={onSubmit} noValidate validated={validated} data-testid="contact-form">
      {FORM_FIELDS.map(field => {
        return (
          <Form.Group className="mb-3" key={field.name} controlId={field.label}>
            <Form.Label>{field.label}</Form.Label>
            <Form.Control
              name={field.name}
              type={field.type}
              required={field.required}
              as={field.component ?? 'input'}
              rows={field.rows ?? 1}
              placeholder={field.placeholder}
              onChange={onChange}
              value={inputValues[field.name]}
            />
            <Form.Control.Feedback type="invalid">
              Please {field.placeholder.toLowerCase()}.
            </Form.Control.Feedback>
          </Form.Group>
        )
      })}
      <ReCAPTCHA
        ref={recaptchaRef}
        size="invisible"
        sitekey={process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY}
        onChange={onReCAPTCHAChange}
      />
      <Button
        variant="primary"
        type="submit"
        disabled={btnLoading}
        className="w-100 mt-3"
      >
        {btnLoading ? 'Submitting...' : 'Submit'}
      </Button>
      <SuccessModal
        showModal={showModal}
        onClose={onClose}
      />
    </Form>
  )
}
