import React, { useState } from 'react';
import Link from 'next/link';
import Button from 'react-bootstrap/Button';
import ListGroup from 'react-bootstrap/ListGroup';
import SearchInput from 'components/SearchInput';
import { formatMoney } from 'utils/utils';

export default function OrderList({ allOrders }) {
  const [searchTerm, setInput] = useState('');
  const [orders, setOrders] = useState(allOrders);

  const searchInput = () => {
    const filteredOrders = allOrders.filter(({ reference }) =>
      reference.toLowerCase().includes(searchTerm.toLowerCase())
    );

    setOrders(filteredOrders);
  }

  const onChange = (event) => {
    setInput(event.target.value);
  }

  return (
    <div className="mt-5">
      <div className="d-flex justify-content-between">
        <SearchInput
          placeholder="Enter order reference number here"
          onChange={onChange}
          value={searchTerm}
        />
        <Button
          className="ml-3"
          variant="primary"
          onClick={searchInput}
        >Search</Button>
      </div>
      {orders.length ? (
        <ListGroup className="mt-5">
          {orders.map(({ reference, delivered, cost }) => {
            return (
              <Link key={reference} href={`/orders/${reference}`} passHref>
                <ListGroup.Item
                  className="d-flex justify-content-between cursor-pointer"
                  role="button"
                  data-testid="order-card"
                >
                  <div>
                    <div className="font-weight-bold">{reference}</div>
                    <div style={{ fontSize: 14 }}>{delivered ? 'Delivered' : 'Not delivered'}</div>
                  </div>
                  <div>{formatMoney(cost)}</div>
                </ListGroup.Item>
              </Link>
            )
          })}
        </ListGroup>
      ) : (
        <div className="mt-5 text-center">
          <div className="font-weight-bold">No orders</div>
        </div>
      )}
    </div>
  );
}
