import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import { formatMoney } from 'utils/utils';

export default function OrderDetails({ order }) {
  const { reference, delivered, cost, date } = order;

  return (
    <Card className="mt-5 py-2">
      <div className="font-weight-bold mb-2 pl-3">Order reference: {reference}</div>
      <Container>
        <Row>
          <Col sm>
            <div>Order date</div>
            <div style={{ fontSize: 14 }}>{new Date(date).toLocaleDateString()}</div>
          </Col>
          <Col sm>
            <div>Status</div>
            <div style={{ fontSize: 14 }}>{delivered ? 'Delivered' : 'Not delivered'}</div>
          </Col>
          <Col sm>
            <div>Cost</div>
            <div style={{ fontSize: 14 }}>{formatMoney(cost)}</div>
          </Col>
        </Row>
      </Container>
    </Card>
  );
}
