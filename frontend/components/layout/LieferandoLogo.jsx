import Image from 'next/image'
import { lieferandoLogo } from './layout.module.css';

export default function LieferandoLogo() {
  return (
    <div className={lieferandoLogo}>
      <Image src="/lieferando.png" alt="Lieferando logo" width={150} height={30} />
    </div>
  )
}
