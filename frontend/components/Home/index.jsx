import React, { useState } from 'react';
import Link from 'next/link';
import Card from 'react-bootstrap/Card';
import { cardContainer } from './home.module.css';
import SearchInput from 'components/SearchInput';

const OPTIONS = [
  {
    name: 'Contact Form',
    route: '/contact-form',
  },
  {
    name: 'Where is my Order?',
    route: '/orders',
  },
];

export default function Home() {
  const [searchTerm, setInput] = useState('');

  const onChange = (event) => {
    setInput(event.target.value);
  }

  return (
    <div>
      <h1 className="mb-5 text-center">Help Center</h1>
      <SearchInput onChange={onChange} value={searchTerm} />
      <div className="d-flex mt-5 flex-wrap justify-content-between">
        {OPTIONS.map(({ name, route }) => {
          return (
            <Link key={name} href={route} passHref>
              <Card className={`${cardContainer} mb-2`} data-testid="option-card">
                <Card.Text>
                  {name}
                </Card.Text>
              </Card>
            </Link>
          )
        })}
      </div>
    </div>
  );
}
