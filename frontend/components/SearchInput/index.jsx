import React from 'react';
import Image from 'next/image';
import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';

export default function SearchInput({ placeholder, value, onChange }) {
  return (
    <InputGroup>
      <InputGroup.Text>
        <Image src="/search-icon.svg" alt="search icon" width={20} height={20} />
      </InputGroup.Text>
      <Form.Control
        placeholder={placeholder}
        onChange={onChange}
        value={value}
      />
    </InputGroup>
  );
}

SearchInput.propTypes = {
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.string,
}

SearchInput.defaultProps = {
  placeholder: 'Search',
  value: '',
}
