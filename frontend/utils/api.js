const BASE_URL = 'http://backend:3000/api';

export function submitContactForm(form) {
  return new Promise((resolve) => setTimeout(resolve, 2000));
}

export async function getOrders() {
  const response = await fetch(`${BASE_URL}/orders`);

  const data = await response.json();
  return data;
}

export async function postPageView(payload) {
  const response = await fetch(
    `${BASE_URL}/page-view`,
    {
      method: 'POST',
      body: JSON.stringify(payload)
    }
  )

  const data = await response.json();
  return data;
}
